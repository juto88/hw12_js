
// document.addEventListener("keydown", function(event) {
//     if (event.key === "Enter") {
//         document.getElementById("enter").style.backgroundColor = "blue";
//     }
//     else {
//         (event.key != "Enter")
//         document.getElementById("enter").style.backgroundColor = "black";
//     }
// });

// document.addEventListener("keydown", function(event) {
//     if (event.key === "s" || event.key === "S") {
//         document.getElementById("myButtonS").style.backgroundColor = "blue";
//     }
//         else {
//             (event.key != "s"|| event.key != "S")
//             document.getElementById("myButtonS").style.backgroundColor = "black";
//         }
// });
// document.addEventListener("keydown", function(event) {
//     if (event.key === "e" || event.key === "E") {
//         document.getElementById("myButtonE").style.backgroundColor = "blue";
//     }
//     else {
//         (event.key != "e"|| event.key != "E")
//         document.getElementById("myButtonE").style.backgroundColor = "black";
//     }
// });
// document.addEventListener("keydown", function(event) {
//     if (event.key === "o" || event.key === "O") {
//         document.getElementById("myButtonO").style.backgroundColor = "blue";
//     }
//     else {
//         (event.key != "o"|| event.key != "O")
//         document.getElementById("myButtonO").style.backgroundColor = "black";
//     }
// });
// document.addEventListener("keydown", function(event) {
//     if (event.key === "n" || event.key === "N") {
//         document.getElementById("myButtonN").style.backgroundColor = "blue";
//     }
//     else {
//         (event.key != "n"|| event.key != "N")
//         document.getElementById("myButtonN").style.backgroundColor = "black";
//     }
// });
// document.addEventListener("keydown", function(event) {
//     if (event.key === "l" || event.key === "L") {
//         document.getElementById("myButtonL").style.backgroundColor = "blue";
//     }
//     else {
//         (event.key != "l"|| event.key != "L")
//         document.getElementById("myButtonL").style.backgroundColor = "black";
//     }
// });
// document.addEventListener("keydown", function(event) {
//     if (event.key === "z" || event.key === "Z") {
//         document.getElementById("myButtonZ").style.backgroundColor = "blue";
//     }
//     else {
//         (event.key != "z"|| event.key != "Z")
//         document.getElementById("myButtonZ").style.backgroundColor = "black";
//     }
// });


const buttons = { // Створюємо об'єкт, що містить відповідні елементи і ключі
    Enter: "enter",
    S: "myButtonS",
    E: "myButtonE",
    O: "myButtonO",
    N: "myButtonN",
    L: "myButtonL",
    Z: "myButtonZ",
  };
 
    let lastButton = null;  // Зберігаємо останню натиснуту кнопку
  
    document.addEventListener("keydown", function (event) { // Додаємо слухач подій на батьківський елемент
    const key = event.key.toUpperCase(); // Перетворюємо клавішу на верхній регістр
  

    if (key in buttons) { // Перевіряємо, чи натиснута клавіша відповідає одній з кнопок
      const buttonId = buttons[key];
      const button = document.getElementById(buttonId);
  
      
      if (lastButton && lastButton.id !== button.id) { // Перевіряємо, чи остання натиснута кнопка відмінна від поточної
        lastButton.style.backgroundColor = "black";
      }
  
      button.style.backgroundColor = "blue";
      lastButton = button;
    }
  
    
    if (event.key === "Enter") { // Перевіряємо, чи натиснута клавіша "Enter"
      const enterButton = document.getElementById(buttons.Enter);
  
      
      if (lastButton && lastButton.id !== enterButton.id) { // Перевіряємо, чи остання натиснута кнопка відмінна від кнопки "Enter"
        lastButton.style.backgroundColor = "black";
      }
  
      enterButton.style.backgroundColor = "blue";
      lastButton = enterButton;
    }
  });
  